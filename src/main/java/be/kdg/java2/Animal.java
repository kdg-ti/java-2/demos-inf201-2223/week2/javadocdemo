package be.kdg.java2;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void eat(){
        System.out.println("njam njam");
    }

    @Override
    public String toString() {
        return name;
    }
}
