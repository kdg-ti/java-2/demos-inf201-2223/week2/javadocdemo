package be.kdg.java2;

/**
 * Represent a real dog
 * @author Hans Vochten
 * @version 1.1
 */
public class Dog extends Animal{
    public Dog(String name) {
        super(name);
    }

    public void bark(){
        System.out.println("Woef!");
    }
}
