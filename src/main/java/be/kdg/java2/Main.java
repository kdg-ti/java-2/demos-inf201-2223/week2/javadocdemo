package be.kdg.java2;

import java.util.HashMap;

/**
 * Main class of the application
 */
public class Main {

    /**
     * The start method of the application
     * @param args startup parameters
     */
    public static void main(String[] args) {
        var catMap = new HashMap<Integer, Cat>();
        catMap.put(1, new Cat("minous"));
        catMap.put(2, new Cat("poes"));
        for (var entry: catMap.entrySet()) {
            System.out.println(entry);
        }
    }
}
