package be.kdg.java2;

public class Cat extends Animal{

    public Cat(String name) {
        super(name);
    }

    /**
     * Makes the cat scream on the standard output
     * @param decibels the amount of decibels
     * @return returns 12
     */
    public int scream(int decibels){
        System.out.println("WHI");
        for (int i = 0; i < decibels; i++) {
            System.out.print("I");
        }
        return 12;
    }
}
