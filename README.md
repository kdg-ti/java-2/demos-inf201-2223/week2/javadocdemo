# javadocdemo

var en javadoc demo tijdens de les
1) In de main: voorbeeld van het gebruik van het var keyword. Vooral in combinatie met generic classes nuttig.
2) Hier en daar commentaar toegevoegd. Demo van het gebruik van javadoc annotations zoals @return @param @author etc
3) Je kan gradle vragen om de javadoc documentatie te genereren: gradlew javadoc